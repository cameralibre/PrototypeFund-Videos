About [Prototype Fund](https://prototypefund.de/)

*Prototype Fund is a project of the [Open Knowledge Foundation Germany](https://okfn.de/), which is funded by the German Federal Ministry of Education and Research ([BMBF](https://www.bmbf.de/en/index.html)). Individuals and small teams receive a grant to test and develop open source tools and applications in the fields of civic tech, data literacy, data security and others.*

*The Prototype Fund team support the candidates in their application process, keeping things as unbureaucratic as possible and adjusting it to the needs of software developers, hackers and creatives. In short: the Prototype Fund brings iterative software development and government innovation funding together.*

*In the next three years about 40 projects will be funded. In total, the BMBF will grant 1.2 million euros in support of these projects.*

The video is based upon the design of the Prototype Fund identity and website, developed by the wonderful team at [Rainbow Unicorn](http://rainbow-unicorn.com/)

While the exported video is © Open Knowledge Foundation Deutschland licensed under [CC-BY](https://creativecommons.org/licenses/by/4.0/), these project files are © [Sam Muirhead](http://cameralibre.cc), licensed under [CC-BY-SA](https://creativecommons.org/licenses/by-sa/4.0/).

The font used in the animation and website is a proprietary font, [Agipo](http://radimpesko.com/fonts/agipo), which unfortunately cannot be included in this repository.

The music track, 'Prototype Fund' was composed and recorded by Javier Suarez (Jahzzar) of [BetterWithMusic.com](http://betterwithmusic.com), and is licensed [CC-BY-SA](https://creativecommons.org/licenses/by-sa/4.0/) Jahzzar.

## Project Structure


**/gif/**

Animated gifs exported from each Synfig project file. This filetype is not suitable for further editing.

**/music/**  
The track 'Prototype Fund' CC-BY-SA Jahzzar, used in the teaser video, in lossless format.

**/sif/**

**The 30-second animated video is made up of 11 separate Synfig animation project files (.sif):**

01 PP Opening.sif  
![01 PP Opening](https://media.giphy.com/media/l2Sq3IVY5ZbYzUURq/giphy.gif)

02 PF Logo.sif  
![02 PF Logo](https://media.giphy.com/media/l2Sq4ou0N32ubDQac/giphy.gif)

03 PushPull-IfElse.sif  
![03 PushPull-IfElse](https://media.giphy.com/media/l2Sq58woMzQezqfni/giphy.gif)

04 IfElse-CopyPaste.sif  
![04 IfElse-CopyPaste](https://media.giphy.com/media/l2SpTOxDE9LcWQXOU/giphy.gif)

05 CopyPaste-ReadWrite.sif  
![05 CopyPaste-ReadWrite](https://media.giphy.com/media/26ueYzes9SoSjUS1W/giphy.gif)

06 ReadWrite-TrialError.sif  
![06 ReadWrite-TrialError](https://media.giphy.com/media/l2SpYpmwf7xSyraaA/giphy.gif)

07 TrialError-DonePerfect.sif  
![07 TrialError-DonePerfect](https://media.giphy.com/media/26uf7WwlMtNyj34GI/giphy.gif)

08 DonePerfect.sif  
![08 DonePerfect](https://media.giphy.com/media/26ufe4GNqXYV8fS7e/giphy.gif)

09 1-2 Millionen fuer OS.sif   
![09 1-2 Millionen fuer OS](https://media.giphy.com/media/l2Sq79nYD53oQHbUs/giphy.gif)

10 jetzt bewerben.sif   
![10 jetzt bewerben](https://media.giphy.com/media/26uf04ajYcKgZwjqo/giphy.gif)

11 PrototypeFund-de.sif   
![11 PrototypeFund-de](https://media.giphy.com/media/d31w1VAD9K5bgBbO/giphy.gif)


**In addition, each of the 5 word-pair animations were made into looping GIFs:**

Done-Perfect_GIF.sif   
![Done-Perfect_GIF](https://media.giphy.com/media/26uf7ThHiYI4vcMBW/giphy.gif)

If-Else_GIF.sif   
![If-Else_GIF](https://media.giphy.com/media/26ufkv7UPIglcE3S0/giphy.gif)

Push-Pull_GIF.sif   
![Push-Pull_GIF](https://media.giphy.com/media/l2SqgmFehQWdliuc0/giphy.gif)

Read-Write_GIF.sif   
![Read-Write_GIF](https://media.giphy.com/media/26ufcGUdRNJvPwAdW/giphy.gif)

Trial-Error_GIF.sif  
![Trial-Error_GIF](https://media.giphy.com/media/26uflbjIUlMpyeM4U/giphy.gif)


**/svg/**

Vector images for layout of each scene, made in Inkscape before being exported as .sif for animation in Synfig.

**/text files/**

Edit notes and transcripts

**/video editing files/**

Kdenlive and Shotcut project files. [Kdenlive](http://kdenlive.org) is a professional level NLE, currently available for Linux only. [Shotcut](http://shotcut.org) is somewhat more basic, but easy to use, stable, and is very easy to install on Mac, Windows or Linux.

**/webm/**

Exported video files with audio. WebM is a highly compressed format, suitable for sharing easily (even via email!)  
The ProRes encoded .mov file is virtually lossless and can be re-edited without significant generation loss.

