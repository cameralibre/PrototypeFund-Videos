### Cut length: 00:03:24:19

00:00:49

My name's Karissa McKelvey, I'm an open source programmer and Ph.D drop out. 
I studied the relationship between online communication and political events. 

00:01:04

I got really frustrated with the data problems I was having and I started getting involved in data tools and visualization. 
And that has led me to do more open source data tools. And now I'm grant-funded and nonprofit and traveling the world doing cool data stuff. 

### What is DAT?
DAT is a tool for sharing data. 

00:01:42

We focus on keeping data open and reusable, reproducible. 

So it's kind of like Git but we really have good defaults for large data sets, and like, randomly accessing data within a big data set. 
So it's kind of, a really nice way to do distributed filesharing. 

### What is a use cases for DAT?

00:02:30

If a data analyst goes to a data portal and wants to study water quality. 
They might get a data set from that. 
They might go to another data portal

and then they go to another data portal... 
What happens if one of those 100 data portals that they go to changes their data. 
They won't actually know unless they go back to the data portal and look for the data again. 
 
00:03:10

W e think that there is a serious problem in the way that people track and manage changes to datasets, specifically in open data. 

So someone might turn the dataset that they're using on a data portal into a DAT and so instead of having to go to the website every time you just say "Oh are there any updates today?" 
And it's more like a 'pull' model.
 
### How has DAT developed over time?

00:05:38

So DAT first started as a prototype project funded by the Knight foundation so  it $50000 for six months of work, and it first started as a data sharing tool for open government and journalism. 

00:06:40

So this is like a third version and we've- this is our third grant as well. 
 
### What do you think are the strengths of public funding? 

00:12:44

The strength of public funding is that it's publicly funded and you're probably doing something that people actually need in the real world, that the market wouldn't do. So corporations are really good at doing something that people will buy. But that's not necessarily something that people need. 

00:13:23

The most exciting thing that motivates me is I'm able to work on projects that just can't be solved in the market.


### What advice would you give to someone was starting a new open source software project?

00:08:35

Start early and publish often, release often and test often. 

Don't develop in a vacuum in the corner of your room, go outside - anywhere outside - like coffee shop, like library like wherever,  and just like to talk about it, and get people to use it, even if it's just like really simple. 
  
00:09:04

Because if you have a good idea, chances are that as long as you have something that kind of works she'll start getting other people who want to help. 
And then you can start building a team. And then once you start building a team you know you can move a lot faster. So that'd be like my first thing is just start. 
Just go. 
Just start doing it and talking to people about it even if it's not 100 percent working.  
