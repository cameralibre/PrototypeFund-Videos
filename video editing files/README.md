### FILETYPES
Both types of editing project files (.mlt & .kdenlive) are XML-based [MLT Playlists](https://www.mltframework.org/)

[EDL](https://en.wikipedia.org/wiki/Edit_decision_list) is the video industry's poorly designed, poorly implemented attempt at a standard. [Blender](https://www.blender.org/) can open EDLs from Shotcut, your mileage may vary with other programs.

.kdenlive files can be opened with [Kdenlive](http://kdenlive.org). (Only available on Linux at this stage) 

Shotcut .mlt playlists can be opened with [Shotcut](http://shotcut.org).
They can also be opened with Kdenlive, following these instructions:

1. copy the .mlt file, and paste in ~/.local/share/kdenlive/library (this is where Kdenlive stores clips for [exchange between projects](https://thediveo-e.blogspot.de/2016/03/kdenlive-ui-copy-paste-between-projects.html))

2. open a Kdenlive project, open the Library Panel (View>Library), locate the .mlt file you want, and click on the plus sign (add Clip to Project)

3. The clip will be visible in your Project Bin. Drag it into the timeline. It will be a single monolithic file at this stage.

4. Go to Timeline>Current Clip>Expand Clip to make it editable and reveal the cut clips.


At this stage I don't know how to smoothly import a Kdenlive Library Clip (also a .mlt file) into Shotcut for editing. I'm working on it :) 

**Karissa McKelvie - DAT.mlt** is the first Prototype Fund video interview, requires the [raw video file](https://drive.google.com/open?id=0B1MT-Mf2e1CpR2xmZDZ6bDBwNjg)

**Vorlageentwurf.kdenlive** is the preliminary draft for a Prototype Fund video template.  


**PrototypeFund.kdenlive** is the 30-second teaser video project, it will require each syfig project to be rendered as PNG images into its corresponding folder:

*/png/1-2millionen/*

*/png/copypaste-readwrite/*

*/png/DonePerfect/*

*/png/IfElse-CopyPaste/*

*/png/jetztbewerben/*

*/png/logoguides/*

*/png/PP/*

*/png/prototypefundde/*

*/png/prototypeFund.png/*

*/png/pushPull-IfElse/*

*/png/ReadWrite-TrialError/*

*/png/TrialError-DonePerfect/*

![Render Dialog](http://www.cameralibre.cc/wp-content/uploads/render.png)

